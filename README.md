# TP BINOME

A) Créer un projet sur gitlab en colab.

B) La répartition des  tâches ainsi que l'avancement de celles-ci doivent apparaître en temps réel dans le tableau Nextcloud correspondant au projet.

C) Créer un script permettant l installation de l'environnement suivant sur une debian vierge: <br>
1. Un répertoire workspace a la racine /<br>
2. Git <br>
3. Python <br>
4. Docker <br>

D) Remplir le readme en suivant la nomenclature exemple:

--------
    Session 2021 Pass Numérique Pro spécialité technicien Devops

    Nom 1 Prénom 1
    Nom 2 Prénom 2

    lien tableau projet: http://exemple/tableau/projetScript

    sujet: Créer un script d'installation d'un envirronement de travail selon le cahier des charges
        client.


    O.S base: Debian10 buster

    Modules complémentaires: Git, Python, Docker, un répertoire de travail avec les accès utilisateur suffisants
    doit figurer au niveau de la racine --> /workspace
--------

E) Pousser le tout sur gitlab 
